

params.bamDir

bam_channel = Channel.fromPath(params.bamDir + "/*.bam")


process samtool {
    
    input:
    file bam from bam_channel

    script:
    """
    samtools view -S ${params.bamDir}/${bam}
    """
}